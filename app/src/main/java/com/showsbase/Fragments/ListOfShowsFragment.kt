package com.showsbase.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.showsbase.R
import com.showsbase.adapter.ShowsAdapter
import com.showsbase.ViewModel.ListOfShowsViewModel
import kotlinx.android.synthetic.main.list_of_shows_fragment.*

class ListOfShowsFragment : Fragment(){

    private lateinit var viewModel: ListOfShowsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_of_shows_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val genres = resources.getStringArray(R.array.genres)
        viewModel = ViewModelProvider(this).get(ListOfShowsViewModel::class.java)
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.genres,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_genres.adapter = adapter
        }
        spinner_genres.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                viewModel.loadData(genres[position])
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
        viewModel.shows.observe(viewLifecycleOwner, Observer { shows ->
            recycler_view_shows.apply {
                layoutManager = GridLayoutManager(requireContext(), 2)
                adapter = ShowsAdapter(shows)
            }
        })
    }
}
