package com.showsbase.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import com.bumptech.glide.Glide
import com.showsbase.R
import com.showsbase.network.ShowsModel
import kotlinx.android.synthetic.main.show_details_fragment.*

class ShowDetails : Fragment() {

    lateinit var shows: ShowsModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        shows = requireArguments().getParcelable("show")!!

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.show_details_fragment, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(view.context)
            .load(shows.image?.original)
            .into(poster)
        detail_name.text = shows.name
        txt_details_summary.text = HtmlCompat.fromHtml(shows.summary!!, HtmlCompat.FROM_HTML_MODE_LEGACY)
        txt_details_original_language.text = " Original language: ${shows.language}"
        txt_details_premiered.text = "Premiered: ${shows.premiered}"
//        txt_details_runtime.text = "Runtime: ${shows.runtime}"
        txt_details_genres.text = "Genres: ${shows.genres?.joinToString { it }}"
        txt_details_rate.text = "Rate: ${shows.rating?.average}/10"
        txt_details_status.text = "Status: ${shows.status}"
    }
}