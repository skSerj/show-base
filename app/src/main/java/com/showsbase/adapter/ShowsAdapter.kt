package com.showsbase.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.showsbase.R
import com.showsbase.network.ShowsModel
import kotlinx.android.synthetic.main.item_show.view.*

class ShowsAdapter(private val shows: List<ShowsModel>) :
    RecyclerView.Adapter<ShowsAdapter.ShowsHolder>() {

    override fun getItemCount() = shows.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowsHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_show, parent, false)
        return ShowsHolder(itemView)
    }

    override fun onBindViewHolder(holder: ShowsHolder, position: Int) {
        holder.bind(shows[position])
    }


    class ShowsHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(shows: ShowsModel) {
            itemView.show_name.text = shows.name
            itemView.ratingBar.rating = shows.rating?.average ?: 0F
            Glide.with(itemView.context)
                .load(shows.image?.medium)
                .into(itemView.show_image)
            itemView.item_root.setOnClickListener(
                Navigation.createNavigateOnClickListener(R.id.action_listOfShowsFragment_to_showDetails, bundleOf("show" to shows))
            )
        }
    }
}
