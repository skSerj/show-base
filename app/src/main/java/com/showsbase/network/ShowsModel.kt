package com.showsbase.network

import android.os.Parcel
import android.os.Parcelable
import java.util.ArrayList

data class ShowsModel(
    var id: Int,
    var url: String?,
    var name: String?,
    var type: String?,
    var language: String?,
    var genres: ArrayList<String>?,
    var status: String?,
    var runtime: Int,
    var premiered: String?,
    var officialSite: String?,
    var rating: Rating?,
    var weight: Int,
    var externals: Externals?,
    var image: Image?,
    var summary: String?,
    var updated: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.createStringArrayList(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(Rating::class.java.classLoader),
        parcel.readInt(),
        parcel.readParcelable(Externals::class.java.classLoader),
        parcel.readParcelable(Image::class.java.classLoader),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(url)
        parcel.writeString(name)
        parcel.writeString(type)
        parcel.writeString(language)
        parcel.writeStringList(genres)
        parcel.writeString(status)
        parcel.writeInt(runtime)
        parcel.writeString(premiered)
        parcel.writeString(officialSite)
        parcel.writeParcelable(rating, flags)
        parcel.writeInt(weight)
        parcel.writeParcelable(externals, flags)
        parcel.writeParcelable(image, flags)
        parcel.writeString(summary)
        parcel.writeInt(updated)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ShowsModel> {
        override fun createFromParcel(parcel: Parcel): ShowsModel {
            return ShowsModel(parcel)
        }

        override fun newArray(size: Int): Array<ShowsModel?> {
            return arrayOfNulls(size)
        }
    }
}

data class Externals(
    var tvrage: Int,
    var thetvdb: Int,
    var imdb: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(tvrage)
        parcel.writeInt(thetvdb)
        parcel.writeString(imdb)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Externals> {
        override fun createFromParcel(parcel: Parcel): Externals {
            return Externals(parcel)
        }

        override fun newArray(size: Int): Array<Externals?> {
            return arrayOfNulls(size)
        }
    }
}

data class Image(
    var medium: String?,
    var original: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(medium)
        parcel.writeString(original)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Image> {
        override fun createFromParcel(parcel: Parcel): Image {
            return Image(parcel)
        }

        override fun newArray(size: Int): Array<Image?> {
            return arrayOfNulls(size)
        }
    }
}

data class Links(
    var self: List<Self>,
    var previousepisode: List<Previousepisode>
)


data class Previousepisode(
    var href: String
)

data class Self(
    var href: String
)

data class Rating(
    var average: Float?
): Parcelable {
    constructor(parcel: Parcel) : this(parcel.readValue(Float::class.java.classLoader) as? Float) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(average)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Rating> {
        override fun createFromParcel(parcel: Parcel): Rating {
            return Rating(parcel)
        }

        override fun newArray(size: Int): Array<Rating?> {
            return arrayOfNulls(size)
        }
    }
}