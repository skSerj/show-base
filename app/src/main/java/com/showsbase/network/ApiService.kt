package com.showsbase.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

object ApiService {
    private const val END_POINT = "http://api.tvmaze.com/"
    private val showsApi: ShowApi

    suspend fun getPopularShow() = showsApi.popularShows()
    suspend fun getShowsByGenre(genre: String) = showsApi.showsByGenre(genre)

    interface ShowApi {
        @GET("shows")
        suspend fun popularShows(): Response<List<ShowsModel>>

        @GET("search/shows")
        suspend fun showsByGenre(
            @Query("q") genre: String): Response<List<ShowsModel>>
    }

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(END_POINT)
            .client(client)
            .build()
        showsApi = retrofit.create(
            ShowApi::class.java)
    }
}