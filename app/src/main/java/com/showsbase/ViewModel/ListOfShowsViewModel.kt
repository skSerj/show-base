package com.showsbase.ViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.showsbase.network.ApiService
import com.showsbase.network.ShowsModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ListOfShowsViewModel : ViewModel() {


    private val _shows = MutableLiveData<List<ShowsModel>>()
    val shows: LiveData<List<ShowsModel>>
        get() = _shows

    fun loadData(genre: String) {
        GlobalScope.launch {
            try {
                val response = ApiService.getPopularShow()
                if (response.isSuccessful && response.body() != null) {
                    val data = response.body()!!
                    if (genre.contains("All genres")) {
                        _shows.postValue(data)
                    } else {
                        _shows.postValue(data.filter { it.genres!!.contains(genre) })
                    }
                } else {
                    Log.d("Coroutine", "error api")
                }
            } catch (e: Exception) {
                Log.d("Coroutine", "catch: ${e.message}")
            }
        }
    }
}